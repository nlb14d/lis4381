# lis4381 - Mobile Web App Development

## Natalie Bosso

### Assignment 5 Requirements:

*Sub-Heading:*

1. Chapter Questions
2. Screenshots of Server-Side Validation
3. README.md

#### README.md file should include the following items:

* Screenshot of index interface 
* Screenshot of process php interface


#### Assignment Screenshots:

*Screenshot of index interface running:
![Index Page Screenshot](img/index.png)

*Screenshot of fail interface running:
![Failed Screenshot](img/fail.png)
