<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Thu, 07-07-16, 13:45:57 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="My online portfolio that illustrates skills aquired in various courses at Florida State University.">
    <meta name="author" content="Natalie Bosso">
    <link rel="icon" href="favicon.ico">

    <title>lis4381 ~ Simple Calculator</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Note: following file is for form validation. -->
<link rel="stylesheet" href="css/formValidation.min.css"/>

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<?php include_once("../global/nav.php"); ?>

    <div class="container">
        <div class="starter-template">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    
                    <div class="page-header">
                        <?php include_once("global/header.php"); ?> 
                    </div>





<?php 
$result = "";
if(isset($_POST['result'])) $result = $_POST['result'];
class calculator
{
    var $a;
    var $b;

    function checkopration($oprator)
    {
        switch($oprator)
        {
            case 'add':
            return $this->a + $this->b;
            break;

            case 'sub':
            return $this->a - $this->b;
            break;

            case 'multi':
            return $this->a * $this->b;
            break;

            case 'divis':
                return ($this->b == 0) ? "Cannot divide by zero !": 
                $this->a / $this->b;
            
            break;

            case 'expo':
            return pow($this->a, $this->b);
            break; 

            default:
            return "Sorry No command found";
        }   
    }
    function getresult($a, $b, $c)
    {
        $this->a = $a;
        $this->b = $b;
        return $this->checkopration($c);
    }
} 

$cal = new calculator();
if(isset($_POST['submit']))
{   
    $result = $cal->getresult($_POST['n1'],$_POST['n2'],$_POST['oper']);
}
?>

<form method="post">
<table align="center">
    
    <tr>
        <td>Num 1</td>
        <td><input type="text" name="n1"></td>
    </tr>

    <tr>
        <td>Num 2</td>
        <td><input type="text" name="n2"></td>
    </tr>

    
        <input type="radio" name="oper"
        <?php if (isset($oper) && $oper=="add") echo "checked";?>
        value="add">Addition

        <input type="radio" name="oper"
        <?php if (isset($oper) && $oper=="sub") echo "checked";?>
        value="sub">Subtraction

        <input type="radio" name="oper"
        <?php if (isset($oper) && $oper=="multi") echo "checked";?>
        value="multi">Multiplication

        <input type="radio" name="oper"
        <?php if (isset($oper) && $oper=="divis") echo "checked";?>
        value="divis">Division

        <input type="radio" name="oper"
        <?php if (isset($oper) && $oper=="expo") echo "checked";?>
        value="expo">Exponentiation

    

    <tr>
        
        
        <td></td>
        <td><input type="submit" name="submit" value="Calculate"></td>
    </tr>
    
    <tr>
        <td><strong><?php echo $result; ?><strong></td>
    </tr>

</table>
</form>

