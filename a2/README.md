# lis4381 - Mobile Web App Development

## Natalie Bosso

### Assignment 2 Requirements:

*Sub-Heading:*

1. Android Studio Development
2. Screenshots of Application Running (Both Phase 1 & 2)
3. README.md

#### README.md file should include the following items:

* Screenshot of first user interface
* Screenshot of second user interface


#### Assignment Screenshots:

*Screenshot of first interface running:

![First Interface Screenshot](img/first.png)

*Screenshot of second interface running:
![Second Interface Screenshot](img/second.png)



#### Tutorial Links:
*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/nlb14d/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/nlb14d/myteamquotes/ "My Team Quotes Tutorial")
