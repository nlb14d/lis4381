# lis4381 - Mobile Web App Development

## Natalie Bosso

### Assignment Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")

    - Install JDK
    - Install Android
    - Create Git Repository	

2. [A2 README.md](a2/README.md "My A2 README.md file")

    - Running Healthy Recipe App
    - Push to Git Repository
    - Application screenshots
    - Chapter Questions

3. [A3 README.md](a3/README.md "My A3 README.md file")

	- Running Event App
	- Building and Forward Engineering Petstore ERD
	- Assignment Screenshots
	- Chapter Questions

4. [P1 README.md](p1/README.md "My P1 README.md file")

    - Running My Business Card App
    - Assignment Screenshots
    - Chapter Questions    

5. [A4 README.md](a4/README.md "My A4 README.md file")

    - Basic Client-Side Validation
    - Assignment Screenshots
    - Chapter Questions
    
6. [A5 README.md](a5/README.md "My A5 README.md file")

    - Basic Server-Side Validation
    - Assignment Screenshots
    - Chapter Questions

7. [P2 README.md](p2/README.md "My P2 README.md file")

    - Basic Client and Server-Side Validation
    - Assignment Screenshots
    - Chapter Questions 
