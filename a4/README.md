# lis4381 - Mobile Web App Development

## Natalie Bosso

### Assignment 4 Requirements:

*Sub-Heading:*

1. Android Studio Development
2. Screenshots of Client-Side Validation
3. README.md

#### README.md file should include the following items:

* Screenshot of first interface 
* Screenshot of fail interface
* Screenshot of success interface



#### Assignment Screenshots:

*Screenshot of first interface running:

![Main Page Screenshot](img/main.png)

*Screenshot of fail interface running:
![Failed Screenshot](img/fail.png)

*Screenshot of success interface running:
![Success Screenshot](img/success.png)

