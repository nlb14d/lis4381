# lis4381 - Mobile Web App Development

## Natalie Bosso

### Project 2 Requirements:

*Sub-Heading:*

1. Client and Server-Side Validation
2. Screenshots 
3. README.md

#### README.md file should include the following items:

* Screenshot of index
* Screenshot of form interface
* Screenshot of error interface
* Screenshot of rss feed
* Screenshot of carousel



#### Assignment Screenshots:

*Screenshot of index interface:
![First Interface Screenshot](img/index.png)

*Screenshot of form interface:
![Second Interface Screenshot](img/form.png)

*Screenshot of error interface:
![Second Interface Screenshot](img/error.png)

*Screenshot of carousel interface:
![Second Interface Screenshot](img/carousel.png)

*Screenshot of RSS feed:
![Second Interface Screenshot](img/rss.png)

