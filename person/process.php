<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
?>

<!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, intial-scale=1">
            <meta name="description" content="My online portfolio"
            <meta name="author" content="Natalie Bosso">
            <link rel="icon" href="favicon.ico">
            
    <title>Simple Person Class</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" />

<!-- Bootstrap for responsive, mobile-first design. -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous" />

<!-- Custom styles with this template -->
<link href="css/starter-template.css" rel="stylesheet" />

<!-- jQuery DataTables: http://www.datatables.net/ //-->
<!-- jQuery DataTables CDN: https://cdn.datatables.net/ //-->

<link rel="stylesheet" type=""text/css" https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<link rel="stylesheet" type=""text/css" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
        </head>
        
        <body>
            <?php include_once("../global/nav.php");?>
            
            <div class="container">
                <div class="starter-template">
                    <div class="page-header">
                        <?php include_once("global/header.php");?>
                    </div>
        <?php
        require_once("classes/person.php");
        
        $personFname =$_POST['fname'];
        $personLname =$_POST['lname'];
        $personAge =$_POST['age'];
        
        $person1= new Person();
        $person2 = new Person($personFname, $personLname, $personAge);
        ?>
        
        <h2>Simple Person Class</h2>
        
        <div class="table-responsive">
            <table id=myTable" class="table table=striped table-condensed">
                <thead>
                    <tr>
                        <th>FName</th>
                        <th>LName</th>
                        <th>Age</th>
                    </tr>
                </thead>
                <tr>
                    <td><?php echo $person1->GetFname(); ?></td>
                    <td><?php echo $person1->GetLname(); ?></td>
                    <td><?php echo $person1->GetAge(); ?></td>          
                </tr>
                
                <tr>
                    <td><?php echo $person2->GetFname(); ?></td>
                    <td><?php echo $person2->GetLname(); ?></td>
                    <td><?php echo $person2->GetAge(); ?></td>
                </tr>
            </table>
        </div>
              <?php include_once "global/footer.php"; ?>
                </div>
            </div>
            
            <!-- Placed at end of document so pages load faster -->
		<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.3.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<!-- jQuery DataTables: http://www.datatables.net/ //-->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="js/ie10-viewport-bug-workaround.js"></script>

	<script>
	 $(document).ready(function(){
    $('#myTable').DataTable({
        responsive:true
    });
});
	</script>
    
</body>
</html>
    