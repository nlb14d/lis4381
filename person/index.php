<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Thu, 07-07-16, 13:45:57 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="My online portfolio that illustrates skills aquired in various courses at Florida State University.">
    <meta name="author" content="Natalie Bosso">
    <link rel="icon" href="favicon.ico">

    <title>Simple Person Class</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Note: following file is for form validation. -->
<link rel="stylesheet" href="css/formValidation.min.css"/>

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<?php include_once("../global/nav.php"); ?>

    <div class="container">
        <div class="starter-template">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    
                    <div class="page-header">
                        <?php include_once("global/header.php"); ?> 
                    </div>

<h2>Simple Person Class</h2>
<form class="form-horizontal" role="form" method="post" action="process.php">
    <div class="form-group">
        <label class="control-label col-sm-2" for="type">FName:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="fname" id="fname" placeholder="Enter first name">
        </div>
    </div>
    
    <div class="form-group">
         <label class="control-label col-sm-2" for="length">LName:</label>
         <div class="col-sm-10">
            <input type="text" class ="form-control" name="lname" id="lname" placeholder="Enter last name">   
         </div>
        </div>
                
    <div class="form-group"
         <label class="control-label col-sm-2" for="length">Age:</label>
         <div class="col-sm-10">
            <input type="text" class ="form-control" name="age" id="age" placeholder="Enter age">   
         </div>
        </div>
                
        <div class = "form-group">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>        
        </div>
</form>

<?php include_once "global/footer.php";?>

            </div>
        </div>
 <!-- Bootstrap core JavaScript: jQuery necessary for Bootstrap's JavaScript plugins
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
		<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.3.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<!-- jQuery DataTables: http://www.datatables.net/ //-->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="js/ie10-viewport-bug-workaround.js"></script>

	<script>
	 $(document).ready(function(){
    $('#myTable').DataTable({
        responsive:true
    });
});
	</script>
    
</body>
</html>
        

