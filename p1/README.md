# lis4381 - Mobile Web App Development

## Natalie Bosso

### Project 1 Requirements:

*Sub-Heading:*

1. Android Studio Development
2. Screenshots of Application Running (Both Phase 1 & 2)
3. README.md

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of first user interface
* Screenshot of second user interface
* SQL file
* mwb file


#### Assignment Screenshots:

*Screenshot of first interface running:

![First Interface Screenshot](img/firstui.png)

*Screenshot of second interface running:
![Second Interface Screenshot](img/secondui.png)

