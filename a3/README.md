# lis4381 - Mobile Web App Development

## Natalie Bosso

### Assignment 3 Requirements:

*Sub-Heading:*

1. Android Studio Development
2. Screenshots of Application Running (Both Phase 1 & 2)
3. Screenshot of ERD
4. SQL and MWB file
5. README.md

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of first user interface
* Screenshot of second user interface
* SQL file
* mwb file


#### Assignment Screenshots:

*Screenshot of ERD:
![ERD Screenshot](img/erd.png)

*Screenshot of first interface running:

![First Interface Screenshot](img/firstui.png)

*Screenshot of second interface running:
![Second Interface Screenshot](img/secondui.png)

### Assignment links
1. [A3 MWB](files/a3.mwb "My a3.mwb file")
2. [A3 SQL](files/a3.sql "My a3.sql file")



#### Tutorial Links:
*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/nlb14d/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/nlb14d/myteamquotes/ "My Team Quotes Tutorial")
