# lis4381 - Mobile Web App Development

## Natalie Bosso

### Assignment 1 Requirements:

*Sub-Heading:*

1. Configuring Git and Bitbucket
2. Development Installation
3. README.md

#### README.md file should include the following items:

* Bullet-list items
* Screenshot of ampps
* Screenshot of JDK java Hello
* Screenshot Android Studio

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init: creates empty Git repository
2. git status: checks current state of the Git repository
3. git add: updates the index
4. git commit: records changes to repository
5. git push: updates remote refs along with associated objects
6. git pull: fetch from and integrate with another repository or local branch
7. git config: sets config. values for your username, email, file formats, etc.

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/localhost.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/Hello.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/nlb14d/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/nlb14d/myteamquotes/ "My Team Quotes Tutorial")
